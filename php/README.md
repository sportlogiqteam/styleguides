SPORTLOGiQ PHP coding standards
===========================

Code should follow PHPs [PSR-2](https://github.com/php-fig/fig-standards/blob/master/accepted/PSR-2-coding-style-guide.md)

Linting
-------
we use PHP_Codesniffer

Example:
```
sudo apt install php-codesniffer
phpcs --standard=/data/src/styleguides/php/sl.ruleset.xml /data/src/restapi/web/api/dbAPI.php
```

## Install git pre-commit hook

* Download the [pre-commit hook](./hooks/pre-commit)
* Add it to the hooks directory (e.g., `/project/.git/hooks`)
* Make sure that it is executable: `chmod +x /project/.git/hooks/pre-commit`

### What it does
1. Runs php in *lint mode* on every file staged for commit
2. Runs phpcs on every file staged for commit

If problems are found when either *linting* or *phpcsing* the commit
operation will be aborted and feedback will be displayed.

#### Sample Output

```bash
FILE: /home/Antonio/code/php/test/test.php
----------------------------------------------------------------------
FOUND 2 ERRORS AFFECTING 2 LINES
----------------------------------------------------------------------
  5 | ERROR | [x] Opening brace of a class must be on the line after
    |       |     the definition
 10 | ERROR | [x] Expected 1 blank line at end of file; 2 found
----------------------------------------------------------------------
PHPCBF CAN FIX THE 2 MARKED SNIFF VIOLATIONS AUTOMATICALLY
----------------------------------------------------------------------

Time: 30ms; Memory: 6Mb

Fix the error before commit.
```
