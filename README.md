SPORTLOGiQ coding standards
===========================

This repository contains the code standards that are to be used in SPORTLOGiQ code.

It also contains various config scripts fro multiple editors:
vim, eclipse, emacs, etc...

We don't have a SLIQ specific cmake style guide. Please use https://community.kde.org/Policies/CMake_Coding_Style as reference for cmake coding.

Rule of thumb
-------------
- Use spaces not Tabs
- Use a 4 space indentation
- Line length is unlimited (use your judgement)


[Python](python/README.md)
[PHP](php/README.md)
[C++](cpp/README.md)
[CMake](cmake/README.md)

