SPORTLOGiQ Python coding standards
===========================

Code should follow Python's [PEP8](https://www.python.org/dev/peps/pep-0008/)
[Nicer presentation of PEP8](https://pep8.org/)

Exceptions
----------
Line lengths are unlimited. Use your judgement.

Linting
-------
we use pylint

Example:
```
pylint --max-line-length=999 sportlogiq.py
```

Install git pre-commit hook
```
TBA
```

**Note: The following are proposed rules pending internal discussion (Paul M, Feb-2023)**

Documentation and Typing
----------
All functions must use typing in the function signature for arguments / return values.

Function docstrings are recommended, especially for 'difficult' functions, but not required. Difficult is subjective, but can be agreed upon in a code review. 
If you do use docstrings, make sure that it is complete and follows something like the style below (or other functions in your module)
```
      def my_function(var1: type1, var2: type2) -> type3:
	  """ Description of function

      Args:
      var1: type1: Description of var1
      var2: type2: Description of var2

      Returns:
      type3: Description of output
      """
```

Libraries and Imports
----------
Multiple imports from the same library can either be split into multiple lines, or one comma-separated list. Not enforced.

Use pathlib instead of os for file handling. pathlib can generally do anything os can do with additional features.

Miscellaneous
----------
When writing input arguments for scripts, e.g.
```
parser.add_argument("--trackfile", help="Path to the track file.", type=str, required=True)
```
use all lowercase, no delimiters for the arguments (i.e. --trackfile, not --track_file, --trackFile)
This reduces cases of ambiguitiy when delimiters get replaced (i.e. --track-file would get called as args.track_file)