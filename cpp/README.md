SPORTLOGiQ CPP coding standards
===========================

Code should follow Googles CPP [guide](https://google.github.io/styleguide/cppguide.html)
Also [here](https://github.com/cheshirekow/cmake_format)

Linting
-------
we use clang-tidy

Example:
```
sudo apt install clang-tidy

# Minimum
cmake -DCMAKE_CXX_CLANG_TIDY="clang-tidy;-checks=-*,google-*"

# Best
cmake -DCMAKE_CXX_CLANG_TIDY="clang-tidy;-checks=*"
```

Install git pre-commit hook
```
TBA
```
