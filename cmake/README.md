SPORTLOGiQ CMake coding standards
===========================

There is no official standard. We just use a generic formatter

Linting
-------
we use [cmake-format](https://github.com/cheshirekow/cmake_format)

Example:
```
sudo -H pip2 install cmake_format^C
cmake-format -i CMakeLists.txt 
```

Install git pre-commit hook
```
TBA
```
